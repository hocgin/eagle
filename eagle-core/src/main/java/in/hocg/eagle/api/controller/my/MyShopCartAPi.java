package in.hocg.eagle.api.controller.my;


import com.baomidou.mybatisplus.core.metadata.IPage;
import in.hocg.eagle.api.pojo.qo.MyCartItemPagingApiQo;
import in.hocg.eagle.api.service.AppService;
import in.hocg.boot.logging.autoconfiguration.core.UseLogger;
import in.hocg.eagle.basic.pojo.ro.IdRo;
import in.hocg.eagle.basic.result.Result;
import in.hocg.eagle.modules.oms.pojo.qo.cart.CartItemInsertRo;
import in.hocg.eagle.modules.oms.pojo.qo.cart.CartItemUpdateRo;
import in.hocg.eagle.modules.oms.pojo.vo.cart.CartItemComplexVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * [订单模块] 购物车表 前端控制器
 * </p>
 *
 * @author hocgin
 * @since 2020-04-18
 */
@Api(tags = "eagle::购物车")
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
@RequestMapping("/api-mini/my/shop-cart")
public class MyShopCartAPi {
    private final AppService service;

    @UseLogger("分页查询 - 购物车")
    @ApiOperation("分页查询 - 购物车")
    @PostMapping("/_paging")
    public Result<IPage<CartItemComplexVo>> pagingMyCartItem(@Validated @RequestBody MyCartItemPagingApiQo qo) {
        return Result.success(service.pagingMyCartItem(qo));
    }

    @UseLogger("加入我的购物车 - 购物车")
    @ApiOperation("加入我的购物车 - 购物车")
    @PostMapping
    public Result<Void> insertMyCartItem(@Validated @RequestBody CartItemInsertRo ro) {
        service.insertMyCartItem(ro);
        return Result.success();
    }

    @UseLogger("更新我的购物车项 - 购物车")
    @ApiOperation("更新我的购物车项 - 购物车")
    @PutMapping
    public Result<Void> updateMyCartItem(@Validated @RequestBody CartItemUpdateRo ro) {
        service.updateMyCartItem(ro);
        return Result.success();
    }

    @UseLogger("删除我的购物车项 - 购物车")
    @ApiOperation("删除我的购物车项 - 购物车")
    @DeleteMapping
    public Result<Void> deleteMyCartItem(@Validated @RequestBody IdRo qo) {
        service.deleteMyCartItem(qo);
        return Result.success();
    }
}

